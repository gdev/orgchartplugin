package ut.com.trulia.confluence.orgchart;

import org.junit.Test;
import com.trulia.confluence.orgchart.MyPluginComponent;
import com.trulia.confluence.orgchart.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}