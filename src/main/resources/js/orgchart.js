var OrgChart = { 

  init: function(gadget, user) {
    this.gadget = gadget;
    this.setViewer(user);
  
    // load reporting structure
    this.retrieveReportingData(function() {
      // build the tree
      OrgChart.buildTree();
      // fill in manager info
      if (OrgChart.reportingData[user.username] && OrgChart.reportingData[user.username]['manager']) {
        var managerUsername = OrgChart.reportingData[user.username]['manager'];
        if (OrgChart.reportingData[managerUsername]) {
          jQuery('#mymanager').val(OrgChart.reportingData[managerUsername][Employee.FULL_NAME]);
        }
      }
      // build name lookup array
      OrgChart.nameLookup = [];
      jQuery.each(OrgChart.reportingData, function(key, employee) {
        OrgChart.nameLookup.push(employee[Employee.FULL_NAME]);
      });
      jQuery('#mymanager').autocomplete({
        lookup: OrgChart.nameLookup,
        onSelect: function (suggestion) {
            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
        }
      });
      jQuery('#findinput').autocomplete({
        lookup: OrgChart.nameLookup,
        onSelect: function (suggestion) {
            // alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
        }
      });
    });
  
    // set up event handlers
    jQuery('#save').click(OrgChart.save); 
    jQuery('#findbtn').click(OrgChart.findEmployee);
    
    // test & debug
    jQuery('#demospacechart').click(function() {
      SpaceTree.rebuild();
    });
    
    // hide test panel
    jQuery("#test-panel").hide();
  },

  save: function() {
    var val = jQuery('#mymanager').val();
    if (val == '') {
      // special case: no manager
      OrgChart.saveReport(null, OrgChart.userData);
      return;
    }
    
    OrgChart.findUsers(jQuery('#mymanager').val(), function(result) {
      if (!result || !("totalSize" in result)) {
        // something went wrong
        alert("Error: " + result);
        return;
      } else if (result.totalSize == 0) {
        alert('User not found!')
      } else if (result.totalSize > 1) {
        // TODO deal w/multiple matches
      }
      var mgrData = {
        fullName: result.result[0].title,
        username: result.result[0].username
      };
      jQuery('#mymanager').val(mgrData[Employee.FULL_NAME]);
      OrgChart.saveReport(mgrData, OrgChart.userData);
    });
  },
  
  findEmployee: function() {
    OrgChart.findUsers(jQuery('#findinput').val(), function(result) {
      if (!("totalSize" in result)) {
        // something went wrong
        alert("Error: " + result);
        return;
      } else if (result.totalSize == 0) {
        alert('User not found!');
        return;
      } else if (result.totalSize > 1) {
        // TODO deal w/multiple matches
      }
      var userData = result.result[0];
      var userFullName = userData.title;
      var username = userData.username;
      jQuery('#findinput').val(userFullName);
      OrgChart.setFocus(username);
    });    
  },
  
  retrieveReportingData: function(callback) {
    AJS.$.ajax({
      url: "/rest/orgchart/1.0/employee.json",
      type: "GET",
      success: function(response) {
        OrgChart.reportingData = response;
        callback();
      },
      error: function(response) {
        console.log(response);
        alert("Error: " + response);
      }
    });    
  },

  setViewer: function(userData) {
    OrgChart.userData = userData;
    jQuery('#myname').text(userData.fullName);
  },

  buildTree: function() {
    // initialize JIT spacetree 
    var treeData = OrgChart.convertToTreeData(OrgChart.reportingData, OrgChart.userData);
    SpaceTree.init('chart', treeData);
  },

  rebuildTree: function() {
    var treeData = OrgChart.convertToTreeData(OrgChart.reportingData, OrgChart.userData);
    SpaceTree.rebuild(treeData);
  },
  
  convertToTreeData: function(reportingData, startEmployee) {
    // no reporting info yet for this employee
    if (!(startEmployee.username in reportingData)) {
      return {
        id: startEmployee.username,
        name: OrgChart.getNodeContent(startEmployee),
        data: startEmployee,
        children: []
      };
    }
    
    // build tree of Nodes
    var subtrees = {};
    var processed = {};
    var empRoot;    // root of the tree for startEmployee

    function makeSubtree(employee, rootUser) {
      // base case: leaf
      if (!employee.reports) {
        processed[employee.username] = true;
        if (startEmployee.username == employee.username) {
          empRoot = rootUser;
        }
        return new Node(employee);
      } 
      
      // recursively process children
      var subtree = new Node(employee);
      jQuery.each(employee.reports, function(index, childUsername) {
        var childTree;
        if (subtrees[childUsername]) {
          // attach existing subtree
          childTree = subtrees[childUsername];
          delete subtrees[childUsername];
        } else {
          childTree = makeSubtree(reportingData[childUsername], rootUser);
        }
        subtree.children.push(childTree);
        processed[childUsername] = true;
        if (childUsername == startEmployee.username) {
          empRoot = rootUser;
        }
      });
      return subtree;
    }
    
    jQuery.each(reportingData, function(username, employee) {
      if (processed[username]) {
        // already added - skip
        return;
      }
      if (!subtrees[username]) {
        subtrees[username] = makeSubtree(employee, username);
      }
    });
    
    var inSubtree = subtrees[empRoot];
    if (!inSubtree) {
      // this is a hack to work around a bug affecting root emps - TODO fix
      inSubtree = subtrees[startEmployee.username];
    }
    treeData = jQuery.parseJSON(JSON.stringify(inSubtree));
    
    return treeData;
  },

  getNodeContent: function(empData) {
    var html = 
      "<img class='profile-pic' src='" + OrgChart.gadget.getBaseUrl() + empData[Employee.PROFILE_PIC] + "'>" +
      "<div>" +
        "<a href='" + OrgChart.gadget.getBaseUrl() + "/display/~" + empData[Employee.USERNAME] + "' target='_profile'>" +
          "<span class='emp-name'>" + empData[Employee.FULL_NAME] + "</span>" + 
        "</a>" + 
      "</div>" +      
      "<div>" +
        "<a class='email' href='mailto:" + empData[Employee.EMAIL] + "'>" + empData[Employee.EMAIL] + "</a>" +
      "</div>";
    return html;
  },

  findUsers: function(query, callback) {
    AJS.$.ajax({
      url: "/rest/prototype/1/search/user.json?query=" + encodeURIComponent(query),
      type: "GET",
      success: callback,
      error: function(response) {
        alert("Error: " + response);
      }
    });  
  },

  setFocus: function(username) {
    this.focus = username;
    // if node already plotted, simply navigate
    var node = SpaceTree.st.graph.getNode(username);
    if (node) {
      SpaceTree.st.onClick(username);
    } else {
      // else rebuild tree with this node at center
      var treeData = OrgChart.convertToTreeData(this.reportingData, this.reportingData[username]);
      SpaceTree.rebuild(treeData);
    }
  },

  saveReport: function(mgr, subordinate) {    
    var mgrUsername;
    if (mgr) {
      mgrUsername = mgr[Employee.USERNAME];
    } else {
      mgrUsername = null;
    }
    subordinate[Employee.MANAGER] = mgrUsername;

    // post employee data to custom REST endpoint
    AJS.$.ajax({
      url: "/rest/orgchart/1.0/employee.json",
      type: "POST",
      data: JSON.stringify(subordinate),
      contentType: "json",
      headers: {"Content-Type": "application/json"},
      success: function(response) {
        // update reporting data & rebuild tree
        OrgChart.retrieveReportingData(OrgChart.rebuildTree);
      },
      error: function(response) {
        alert("Error: " + response);
      }
    });    
  }
}

var Employee = {
    FULL_NAME: 'fullName',
    USERNAME: 'username',
    EMAIL: 'email',
    MANAGER: 'manager',
    REPORTS: 'reports',
    PROFILE_PIC: 'profilePic'
}

function Node(data) {
  this.id = data.username;
  this.name = OrgChart.getNodeContent(data);
  this.data = data;
  this.children = [];
}


