package com.trulia.confluence.orgchart;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.*;

import com.atlassian.user.User;

public class Employee {

  @XmlElement
  private String username;
    
  @XmlElement 
  private String manager;
 
  @XmlElement
  private String fullName;
    
  @XmlElement 
  private String email;
    
  @XmlElement
  private List<String> reports;
  
  @XmlElement
  private String profilePic;
 
  @XmlElement 
  private String title;
  
  // This private constructor isn't used by any code, but JAXB requires any
  // representation class to have a no-args constructor.
  private Employee() { }
 
  public Employee(String username)
  {
    this.username = username;
  }
  
  public Employee(User user) {
    this.username = user.getName();
    this.email = user.getEmail();
    this.fullName = user.getFullName();
  }

	public String getUsername() {
		return this.username;
	}
	
	public void setFullName(String fullName) {
	  this.fullName = fullName;
	}
	
	public void setEmail(String email) {
	  this.email = email;
	}
	
	public String getManager() {
		return this.manager;
	}
	
	public String getEmail() {
	  return this.email;
	}
	
	public void addReport(String username) {
	  if (this.reports == null) {
	    this.reports = new ArrayList<String>();
	  }
	  if (this.reports.contains(username)) return;
	  this.reports.add(username);
	}
	
	public List<String> getReports() {
	  return this.reports;
	}
	
	public void setProfilePic(String url) {
	  this.profilePic = url;
	}
	
	public String getProfilePic() {
	  return this.profilePic;
	}
}
