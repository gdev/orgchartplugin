package com.trulia.confluence.orgchart;

public interface MyPluginComponent
{
    String getName();
}