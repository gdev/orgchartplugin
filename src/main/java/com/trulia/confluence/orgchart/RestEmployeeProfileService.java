package com.trulia.confluence.orgchart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import com.atlassian.bandana.BandanaContext;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.setup.bandana.KeyedBandanaContext;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.user.User;
import com.atlassian.user.search.page.Pager;
import com.opensymphony.module.propertyset.PropertySet;
 
@Path("/")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class RestEmployeeProfileService {
	
	private static final String KEY_PREFIX = "com.trulia.orgchart.";
	private static final String LIST_KEY   = "employees";
	
	private final BandanaManager bandanaMgr;
	private final KeyedBandanaContext bandanaCtx;
	private final UserAccessor userAccessor;

	public RestEmployeeProfileService(BandanaManager bandanaMgr, UserAccessor userAccessor) {
		this.bandanaMgr = bandanaMgr;
		this.userAccessor = userAccessor;
		// currently using global context
		this.bandanaCtx = ConfluenceBandanaContext.GLOBAL_CONTEXT;
		// TODO use private context vie KeyedBandanaContext
		Object empMap = bandanaMgr.getValue(bandanaCtx, KEY_PREFIX + LIST_KEY);
		if (empMap == null) {
		  empMap = new Hashtable<String, Employee>();
		}
    bandanaMgr.setValue(bandanaCtx, KEY_PREFIX + LIST_KEY, empMap);
	}
		
	@GET
  @Path("employee")
  public Response getEmployees() {
    Map<String, Employee> empMap = (Map<String, Employee>) bandanaMgr.getValue(bandanaCtx, KEY_PREFIX + LIST_KEY);
    // supplement with live confluence data
    Pager<User> userPager = this.userAccessor.getUsers();
    Iterator<User> userIter = userPager.iterator();
    while (userIter.hasNext()) {
      User user = userIter.next();
      Employee emp = empMap.get(user.getName());
      if (emp == null) {
        emp = new Employee(user);
        empMap.put(user.getName(), emp);
      } else {
        emp.setEmail(user.getEmail());
        emp.setFullName(user.getFullName());
      }
      ProfilePictureInfo profilePicInfo = this.userAccessor.getUserProfilePicture(user);
      emp.setProfilePic(profilePicInfo.getDownloadPath());
    }
    return Response.ok(empMap).build();
  }

  @GET
  @Path("employee/{username}")
  public Response getEmployee(@PathParam("username") String username) {
    Map<String, Employee> empMap = (Map<String, Employee>) bandanaMgr.getValue(bandanaCtx, KEY_PREFIX + LIST_KEY);
    Employee emp = empMap.get(username);
    User user = this.userAccessor.getUser(username);
    if (emp == null && user != null) {
      emp = new Employee(user);
    } else {
      emp.setEmail(user.getEmail());
      emp.setFullName(user.getFullName());
    }
    ProfilePictureInfo profilePicInfo = this.userAccessor.getUserProfilePicture(user);
    emp.setProfilePic(profilePicInfo.getDownloadPath());
    return Response.ok(emp).build();
  }

  @POST
  @Path("employee")
  @Consumes(MediaType.APPLICATION_JSON)
  public Response setEmployee(Employee employee) {
  	// store entire list under one key
  	Map<String, Employee> empMap = (Map<String, Employee>) bandanaMgr.getValue(bandanaCtx, KEY_PREFIX + LIST_KEY);
  	// TODO take care of removing from previous manager, if any
  	String mgrUsername = employee.getManager();
  	Employee mgr = null;
  	if (mgrUsername != null) {
      mgr = empMap.get(employee.getManager());
  	}
    empMap.put(employee.getUsername(), employee);
  	if (mgrUsername != null) {
    	if (mgr == null) {
    	  mgr = new Employee(employee.getManager());
    	}
    	mgr.addReport(employee.getUsername());
      empMap.put(mgr.getUsername(), mgr);
  	}
  	bandanaMgr.setValue(bandanaCtx, KEY_PREFIX + LIST_KEY, empMap);
    return Response.ok(employee).build();
    
    // alternative approach: store each employee under own key 
    // bandanaMgr.setValue(bandanaCtx, KEY_PREFIX + employee.getUsername(), employee);
  }
  
  @DELETE
  @Path("employee")
  public Response deleteEmployees() {
    bandanaMgr.setValue(bandanaCtx, KEY_PREFIX + LIST_KEY, new Hashtable<String, Employee>());
    return Response.ok().build();
  }
}
