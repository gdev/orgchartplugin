/**
 * 
 */
package com.trulia.confluence.orgchart;

import java.util.Map;

import org.apache.log4j.Logger;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.plugin.descriptor.web.WebInterfaceContext;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.user.User;

/**
 * @author ggutkin
 *
 */
public class ExtendedProfileAction extends ConfluenceActionSupport {

	private static Logger logger = Logger.getLogger(ExtendedProfileAction.class);
	
	private UserAccessor userAccessor;
	
	/**
	 * 
	 */
	public ExtendedProfileAction() {
		super();
	}

	public String execute() {
		String action = this.getActionName();
		User user = this.getRemoteUser();
		Map<String, Object> context = this.getContext();
		WebInterfaceContext webIntContext = this.getWebInterfaceContext();
		
		logger.info(this.getActionName() + " action called");
		return "success";
	}

	public void setUserAccessor(UserAccessor userAccessor) {
		this.userAccessor = userAccessor;
	}
	
	public void setWebInterfaceManager(WebInterfaceManager webInterfaceManager) {
		this.webInterfaceManager = webInterfaceManager;
	}
}
